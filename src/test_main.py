"""Publishable Articles feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('features/publishable_articles.feature', 'Creating an article doesn\'t publish it')
def test_creating_an_article_doesnt_publish_it():
    """Creating an article doesn't publish it."""


@scenario('features/publishable_articles.feature', 'Explicitly publishing an article publishes it')
def test_explicitly_publishing_an_article_publishes_it():
    """Explicitly publishing an article publishes it."""


@given('An article is created')
def an_article_is_created():
    """An article is created."""
    raise NotImplementedError


@given('it is explicitly published')
def it_is_explicitly_published():
    """it is explicitly published."""
    raise NotImplementedError


@given('it\'s not explicitly published')
def its_not_explicitly_published():
    """it's not explicitly published."""
    raise NotImplementedError


@then('it\'s state is published')
def its_state_is_published():
    """it's state is published."""
    raise NotImplementedError


@then('it\'s state is unpublished')
def its_state_is_unpublished():
    """it's state is unpublished."""
    raise NotImplementedError

